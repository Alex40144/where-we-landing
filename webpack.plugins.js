const webpack = require("webpack");
const path = require("path");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = [
    new ForkTsCheckerWebpackPlugin(),
    new webpack.DefinePlugin({
        "STATIC_PATH": `"${path.join(__dirname, "/static").replace(/\\/g, "\\\\")}"`
    })
];
