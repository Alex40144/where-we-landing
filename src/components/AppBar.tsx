import React, { FC, useState } from "react";
import { Flex, Box, Center } from "@chakra-ui/core";
import { ipcRenderer } from "electron";

const AppBar: FC<{}> = (props) => {
    const [maximized, setMaximized] = useState(false);

    function onMinimize() {
        ipcRenderer.invoke("window-minimize");
    }

    function onClose() {
        ipcRenderer.invoke("window-close");
    }

    function onMaximizeToggle() {
        ipcRenderer.invoke("window-maximize-toggle");
        setMaximized(!maximized);
    }

    return (
        <Flex bg="blue.600" className="app-bar" color="white" height="32px">
            <Box
                fontSize={12}
                px={4}
                flexGrow={1}
                overflow="hidden"
                whiteSpace="nowrap"
                display="flex"
                alignItems="center"
            >
                WHERE WE LANDING
            </Box>
            <Center
                as="button"
                className="app-bar-button"
                px={2}
                flexShrink={0}
                _hover={{ bg: "blue.500" }}
                _focus={{ outline: "none" }}
                onClick={onMinimize}
            >
                <img src="../static/minimize.png" />
            </Center>
            <Center
                as="button"
                className="app-bar-button"
                px={2}
                flexShrink={0}
                _hover={{ bg: "blue.500" }}
                _focus={{ outline: "none" }}
                onClick={onMaximizeToggle}
            >
                {maximized ?
                    <img src="../static/restore.png" /> :
                    <img src="../static/maximize.png" />}
            </Center>
            <Center
                as="button"
                className="app-bar-button"
                px={2}
                flexShrink={0}
                _hover={{ bg: "red.500" }}
                _focus={{ outline: "none" }}
                onClick={onClose}
            >
                <img src="../static/close.png" />
            </Center>
        </Flex>
    );
}

export default AppBar;