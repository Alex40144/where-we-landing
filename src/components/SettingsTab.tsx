import React, { FC, useState } from "react"
import { Box, Heading, FormControl, FormLabel, Input, Button, FormErrorMessage, Center } from "@chakra-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { Formik, FormikHelpers, Field, FormikProps, FieldProps } from "formik";

import { AppState } from "src/store/store";
import { setAccessKey } from "src/store/actions";

type AccessKeyForm = {
    accessKey: string;
}

const SettingsTab: FC<{}> = (props) => {
    const accessKey = useSelector<AppState, string>(state => state.settings.accessKey);
    const dispatch = useDispatch();

    function onSubmit(values: AccessKeyForm, actions: FormikHelpers<AccessKeyForm>) {
        dispatch(setAccessKey(values.accessKey));
    }

    function validateAccessKey(value: string) {
        if (value.length != 16) {
            return "Access key must be 16 characters long.";
        }

        return null;
    }

    return (
        <Box p={4}>
            <Heading size="lg">Access Key</Heading>
            Your data feed is limited to 10 requests per hour, and 40 requests per 5 hours.
            <Formik<AccessKeyForm> initialValues={{ accessKey: accessKey ?? "" }} onSubmit={onSubmit}>
                {(formikProps) => (
                    <form onSubmit={formikProps.handleSubmit}>
                        <Field name="accessKey" validate={validateAccessKey}>
                            {({ field, form }: FieldProps<string, AccessKeyForm>) => (
                                <FormControl isInvalid={!!form.errors.accessKey}>
                                    <FormLabel>Access Key</FormLabel>
                                    <Input {...field} type="password" />
                                    <FormErrorMessage>{form.errors.accessKey}</FormErrorMessage>
                                </FormControl>
                            )}
                        </Field>
                        <Box display="flex" mt={2}>
                            <Button
                                type="submit"
                                colorScheme="blue"
                                disabled={!formikProps.isValid || !formikProps.dirty}
                            >
                                Save
                            </Button>
                            &nbsp;
                            { formikProps.submitCount != 0 && <Center color="green.500">
                                Saved successfully!
                            </Center> }
                        </Box>
                    </form>
                )}
            </Formik>
        </Box>
    );
}

export default SettingsTab;