import React, { FC, useState } from "react";
import { Box, Alert, AlertIcon } from "@chakra-ui/core";
import { useSelector } from "react-redux";

import { AppState } from "src/store/store";
import StartBar from "src/components/map/StartBar";
import IcaoBar from "src/components/map/IcaoBar";

type MapSideBarProps = {
    onOpenIcao: (icao: string) => void;
    onClearMap: () => void;
}

const MapSideBar: FC<MapSideBarProps> = (props) => {
    const [activeBar, setActiveBar] = useState("start");
    const [selectedIcao, setSelectedIcao] = useState("");
    const accessKey = useSelector<AppState, string>(state => state.settings.accessKey);

    let warning;
    if (accessKey == null) {
        warning = (
            <Alert status="error" mb={2}>
                <AlertIcon />
                You have not yet added an Access Key in settings.
            </Alert>
        );
    }

    let bar;
    switch (activeBar) {
        case "start":
            bar = <StartBar onOpenIcao={(icao) => {
                setSelectedIcao(icao);
                setActiveBar("icao");
                props.onOpenIcao(icao);
            }} />;
            break;
        case "icao":
            bar = <IcaoBar onGoHome={() => {
                setActiveBar("start");
                props.onClearMap();
            }} icao={selectedIcao} />
            break;
    }

    return (
        <Box p={2}>
            {warning}
            {bar}
        </Box>
    );
}

export default MapSideBar;