import React, { FC } from "react";
import { Button, Heading, Text, FormControl, FormLabel, FormErrorMessage, Input, InputGroup, InputRightAddon } from "@chakra-ui/core";
import { MdKeyboardArrowLeft } from "react-icons/md";
import { useSelector } from "react-redux";

import { IcaoEntry } from "src/utils/icaos";
import { AppState } from "src/store/store";
import { Formik, Field, FieldProps } from "formik";

type SearchAssignmentsForm = {
    range: string;
    requestLimit: string;
}

type IcaoBarProps = {
    icao: string;
    onGoHome: () => void;
}

const IcaoBar: FC<IcaoBarProps> = (props) => {
    const icaoEntries = useSelector<AppState, Record<string, IcaoEntry>>(
        state => state.icaos.icaoEntries
    );

    const entry = icaoEntries[props.icao];

    function onSearchAssignmentsSubmit(values: SearchAssignmentsForm) {
    }

    return (
        <div>
            <Button
                leftIcon={<MdKeyboardArrowLeft />}
                onClick={props.onGoHome}
                colorScheme="blue"
                variant="outline"
                size="sm"
                width="full"
            >
                Go Back
            </Button>
            <Heading size="lg" mb={-1}>{props.icao}</Heading>
            <Text size="lg">{entry.name}</Text>

            <Heading size="md">Search Assignments</Heading>
            <Formik<SearchAssignmentsForm>
                initialValues={{ range: "50", requestLimit: "2" }}
                onSubmit={onSearchAssignmentsSubmit}
            >
                {(formikProps) => (
                    <form onSubmit={formikProps.handleSubmit}>
                        <Field name="range">
                            {({ field, form }: FieldProps<string, SearchAssignmentsForm>) => (
                                <FormControl isInvalid={!!form.errors.range}>
                                    <FormLabel>Range</FormLabel>
                                    <InputGroup>
                                        <Input {...field} type="text" />
                                        <InputRightAddon children="NM" />
                                    </InputGroup>
                                    <FormErrorMessage>{form.errors.range}</FormErrorMessage>
                                </FormControl>
                            )}
                        </Field>
                        <Field name="requestLimit">
                            {({ field, form }: FieldProps<string, SearchAssignmentsForm>) => (
                                <FormControl isInvalid={!!form.errors.requestLimit}>
                                    <FormLabel>Datafeed Request Limit</FormLabel>
                                    <Input {...field} type="text" />
                                    <FormErrorMessage>{form.errors.requestLimit}</FormErrorMessage>
                                </FormControl>
                            )}
                        </Field>
                        <Button
                            type="submit"
                            colorScheme="blue"
                            width="full"
                            mt={2}
                            disabled={!formikProps.isValid}
                        >
                            Search
                    </Button>
                    </form>
                )}
            </Formik>
        </div>
    );
}

export default IcaoBar;