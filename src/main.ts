import { app, BrowserWindow, ipcMain } from "electron";

declare const MAIN_WINDOW_WEBPACK_ENTRY: any;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) { // eslint-disable-line global-require
    app.quit();
}

const createWindow = (): void => {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        height: 600,
        width: 800,
        minWidth: 200,
        minHeight: 100,
        frame: false,
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindow.removeMenu();

    mainWindow.webContents.openDevTools();

    // and load the index.html of the app.
    mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    // On OS X it"s common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

ipcMain.handle("window-minimize", event => {
    BrowserWindow.fromWebContents(event.sender).minimize();
});

ipcMain.handle("window-maximize-toggle", event => {
    const window = BrowserWindow.fromWebContents(event.sender);
    if (window.isMaximized()) {
        window.restore();
    } else {
        window.maximize();
    }
});

ipcMain.handle("window-close", event => {
    BrowserWindow.fromWebContents(event.sender).close();
});

ipcMain.handle("get-user-data", event => {
    return app.getPath("userData");
})
