import { IcaoEntry } from "src/utils/icaos";

export const SET_ACCESS_KEY = "setAccessKey";
export const SET_ICAO_POSITIONS = "setIcaoPositions";

export type SetAccessKeyAction = {
    type: typeof SET_ACCESS_KEY,
    accessKey: string,
}

export type SettingsAction = SetAccessKeyAction;

export const setAccessKey = (accessKey: string): SetAccessKeyAction => ({
    type: SET_ACCESS_KEY,
    accessKey: accessKey,
});

export type SetIcaoEntriesAction = {
    type: typeof SET_ICAO_POSITIONS,
    icaoEntries: Record<string, IcaoEntry>,
}

export type IcaosAction = SetIcaoEntriesAction;

export const setIcaoPositions = (icaoEntries: Record<string, IcaoEntry>): SetIcaoEntriesAction => ({
    type: SET_ICAO_POSITIONS,
    icaoEntries: icaoEntries,
});
