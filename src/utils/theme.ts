import theme from "@chakra-ui/theme";
import { merge } from "@chakra-ui/utils";

const appTheme = merge({}, theme, {
    styles: {
        global: (props: any) => {
            let global: any = (theme.styles.global as any)(props);
            global.body = { backgroundColor: theme.colors.gray[100] };
            return global;
        }
    }
});

export default appTheme;