import fs from "fs";
import { ipcRenderer } from "electron";

export type Assignment = {
    toIcao: string;
    pay: number;
}

export type StoredSettings = {
    accessKey?: string;
}

export type StoredData = {
    settings?: StoredSettings;
}

export class UserData {
    static async loadIcaoAssignments(): Promise<Record<string, Assignment[]>> {
        const userData: string = await ipcRenderer.invoke("get-user-data");
        try {
            const data = await fs.promises.readFile(
                userData + "/icaoAssignments.json",
                { encoding: "utf8" }
            );
            return JSON.parse(data);
        } catch {
            // Just ignore if we couldn't open the file
            return {};
        }
    }

    static async saveIcaoAssignments(icaoAssignments: Record<string, Assignment[]>) {
        const userData: string = await ipcRenderer.invoke("get-user-data");
        await fs.promises.writeFile(
            userData + "/icaoAssignments.json",
            JSON.stringify(icaoAssignments),
            { encoding: "utf8" }
        );
    }

    static async loadData(): Promise<StoredData> {
        const userData: string = await ipcRenderer.invoke("get-user-data");
        try {
            const data = await fs.promises.readFile(
                userData + "/data.json",
                { encoding: "utf8" }
            );
            return JSON.parse(data);
        } catch {
            // Defaults if we couldn't open the file
            return {};
        }
    }
    
    static async saveData(data: StoredData) {
        const userData: string = await ipcRenderer.invoke("get-user-data");
        await fs.promises.writeFile(
            userData + "/data.json",
            JSON.stringify(data),
            { encoding: "utf8" }
        );
    }
}