const path = require("path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const rules = require("./webpack.rules");
const plugins = require("./webpack.plugins");

rules.push({
    test: /\.css$/,
    use: [
        {
            loader: "style-loader"
        }, {
            loader: "css-loader",
            options: { url: false }
        }
    ],
});

module.exports = {
    module: {
        rules,
    },
    plugins: [
        ...plugins,
        new CopyWebpackPlugin({
            patterns: [{
                from: path.resolve(__dirname, "static"),
                to: path.resolve(__dirname, ".webpack/renderer/static")
            }]
        }),
        new CopyWebpackPlugin({
            patterns: [{
                from: path.resolve(__dirname, "node_modules/leaflet/dist/images"),
                to: path.resolve(__dirname, ".webpack/renderer/static/leaflet")
            }]
        })
    ],
    resolve: {
        extensions: [".js", ".ts", ".jsx", ".tsx", ".css"],
        plugins: [new TsconfigPathsPlugin()],
    },
};
